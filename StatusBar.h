//
// Created by kasperovich on 24.05.2019.
//

#ifndef MYGAME_STATUSBAR_H
#define MYGAME_STATUSBAR_H


#include <cocos/2d/CCNode.h>
#include <algorithm>

#include "Logger.h"
#include "cocos2d.h"

class StatusBar : public cocos2d::Node
{
public:

    static StatusBar* create(std::shared_ptr<Logger> log, std::string userName);

    void initStatusBar(std::shared_ptr<Logger> log, std::string userName);

    void scoreLabelUpdate(size_t countEatenApples);

private:

    std::shared_ptr<Logger> _log;

    cocos2d::DrawNode* _statusBarPanel;
    cocos2d::Label*    _scoreLabel;
    cocos2d::Label*    _userNameLabel;

    void createPanel();

    void createScoreLabel(size_t countEatenApples);
    void createUserNameLabel(std::string userName);
};


#endif //MYGAME_STATUSBAR_H
