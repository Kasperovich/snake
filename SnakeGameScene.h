#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include <deque>
#include <algorithm>
#include <cstdio>
#include <optional>

#include "cocos2d.h"
#include "Snake.h"
#include "Cell.h"
#include "Logger.h"
#include "Records.h"
#include "RecordsScreen.h"
#include "MyMenu.h"
#include "EnterNameScreen.h"
#include "Windows.h"


class SnakeGameScene : public cocos2d::Scene
{
public:

    enum class Screen
    {
        None,
        Menu,
        EnterName,
        Records,
        Game
    };

    static constexpr size_t _formWidth = 50;

    static constexpr size_t _formHeight = 46;

    std::optional<std::string> _userName;

    static cocos2d::Scene* createScene();

    virtual bool init();

    void setScreen(Screen screen);

    void setUserName(std::string);

    std::string getUserName();

    void saveRecord(std::string userName, std::uint32_t scope);


    // implement the "static create()" method manually
    CREATE_FUNC(SnakeGameScene);

private:

    Game*            _game;
    MyMenu*          _menu;
    EnterNameScreen* _enterNameScreen;
    RecordsScreen*   _recordsScreen;

    Records _records;

    Screen _currentScreen = Screen::None;

    std::shared_ptr<Logger> _log;


    void removeScreen(Screen screen);

    void initScreen(Screen screen);
};

#endif // __HELLOWORLD_SCENE_H__
