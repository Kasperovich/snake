//
// Created by kasperovich on 17.06.2019.
//

#include "EnterNameScreen.h"
#include "SnakeGameScene.h"

EnterNameScreen* EnterNameScreen::create(size_t formWidth, size_t formHeight, std::shared_ptr<Logger> log, SnakeGameScene* scene)
{
    EnterNameScreen* enterNameScreen = new (std::nothrow) EnterNameScreen();

    if (enterNameScreen && enterNameScreen->init())
    {
        enterNameScreen->autorelease();

        enterNameScreen->initScreen(formWidth, formHeight, std::move(log), scene);

        return enterNameScreen;
    }
    delete enterNameScreen;
    return nullptr;
}

void EnterNameScreen::initScreen(size_t formWidth, size_t formHeight, std::shared_ptr<Logger> log, SnakeGameScene* scene)
{
    _scene      = scene;
    _log        = std::move(log);
    _formWidth  = formWidth;
    _formHeight = formHeight;

    initTextField();
    initButtons();
}

void EnterNameScreen::initTextField()
{
    _textField = cocos2d::ui::TextField::create("Enter your name", "Arial", 5);
    _textField->setPosition(cocos2d::Vec2(_formWidth / 2, 30));
    _textField->setMaxLength(12);
    _textField->setMaxLengthEnabled(true);

    this->addChild(_textField);
}

void EnterNameScreen::initButtons()
{
    auto backToMenu = [this]
    {
        auto scene = _scene;
        scene->setScreen(SnakeGameScene::Screen::Menu);
    };

    auto startGame = [this]
    {
        auto& userName = _textField->getStringValue();
        if (!userName.empty())
        {
            auto scene = _scene;
            scene->setUserName(userName);
            scene->setScreen(SnakeGameScene::Screen::Game);
        }
    };

    _startGame = initButton(cocos2d::Vec2(_formWidth / 2, 20), "Start", startGame);
    _backToMenu = initButton(cocos2d::Vec2(_formWidth / 2, 15), "Back", backToMenu);
}

cocos2d::ui::Button* EnterNameScreen::initButton(cocos2d::Vec2 position, std::string titleText, std::function<void()> callback)
{
    auto button = cocos2d::ui::Button::create("", "", "");
    button->setTitleText(std::move(titleText));
    button->setTitleFontSize(3);
    button->setPosition(position);

    button->addTouchEventListener([this, callback](Ref* sender, cocos2d::ui::Widget::TouchEventType type)
    {
        if (type == cocos2d::ui::Widget::TouchEventType::ENDED)
        {
            callback();
        }
    });

    this->addChild(button);

    _log->write("add " + titleText + " Button");

    return button;
}