//
// Created by kasperovich on 23.05.2019.
//

#include "MyMenu.h"
#include "SnakeGameScene.h"


MyMenu* MyMenu::create(size_t formWidth, std::shared_ptr<Logger> log, SnakeGameScene *scene)
{
    MyMenu* menu = new (std::nothrow) MyMenu();

    if (menu && menu->init())
    {
        menu->autorelease();

        menu->initScreen(formWidth, scene, log);

        return menu;
    }

    delete menu;
    return nullptr;
}

void MyMenu::initScreen(size_t formWidth, SnakeGameScene* scene, std::shared_ptr<Logger> log)
{
    _scene = scene;
    _log = std::move(log);

    initHeaderWithPosition(cocos2d::Vec2(formWidth / 2, 35));
    initButtons(formWidth);
}

void MyMenu::initButtons(size_t formWidth)
{
    auto enterName = [this]()
    {
        auto scene = _scene;
        scene->setScreen(SnakeGameScene::Screen::EnterName);
    };

    auto records = [this]()
    {
        auto scene = _scene;
        scene->setScreen(SnakeGameScene::Screen::Records);
    };

    auto exit = [this]()
    {
        cocos2d::Director::getInstance()->end();
    };

    _enterName = initButton(cocos2d::Vec2(formWidth / 2, 27.0f), "New game", enterName);
    _records = initButton(cocos2d::Vec2(formWidth / 2, 23.0f), "Records", records);
    _exit = initButton(cocos2d::Vec2(formWidth / 2, 19.0f), "Exit", exit);
}

void MyMenu::initHeaderWithPosition(const cocos2d::Vec2 position)
{
    _menuHeader = cocos2d::Label::createWithSystemFont("SNAKE", "Arial", 6);
    _menuHeader->setPosition(position);
    this->addChild(_menuHeader);

    _log->write("add menu Header");
}

cocos2d::ui::Button* MyMenu::initButton(cocos2d::Vec2 position, std::string titleText, std::function<void()> callback)
{
    auto button = cocos2d::ui::Button::create("", "", "");
    button->setTitleText(std::move(titleText));
    button->setTitleFontSize(3);
    button->setPosition(position);

    button->addTouchEventListener([this, callback](Ref* sender, cocos2d::ui::Widget::TouchEventType type)
    {
        if (type == cocos2d::ui::Widget::TouchEventType::ENDED)
        {
            callback();
        }
    });

    this->addChild(button);

    _log->write("add " + titleText + " Button");

    return button;
}