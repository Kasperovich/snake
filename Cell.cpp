//
// Created by kasperovich on 19.05.2019.
//

#include "Cell.h"

Cell* Cell::create(cocos2d::Color4F color)
{
    Cell* cell = new (std::nothrow) Cell();

    if (cell && cell->init())
    {
        cell->autorelease();

        auto drawNode = cocos2d::DrawNode::create();
        drawNode->drawSolidRect({ 0.0f, 0.0f }, { 1.0f, 1.0f }, color);
        cell->addChild(drawNode);

        return cell;
    }

    delete cell;
    return nullptr;
}