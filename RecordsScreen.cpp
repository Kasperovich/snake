//
// Created by kasperovich on 18.06.2019.
//

#include "RecordsScreen.h"
#include "SnakeGameScene.h"


RecordsScreen * RecordsScreen::create(const std::vector<Records::Note>& notes, size_t formWidth, SnakeGameScene* scene)
{
    RecordsScreen* screen = new (std::nothrow) RecordsScreen();

    if (screen && screen->init())
    {
        screen->autorelease();

        screen->initScreen(notes, formWidth, scene);

        return screen;
    }

    delete screen;
    return nullptr;
}

void RecordsScreen::initScreen(const std::vector<Records::Note>& notes, size_t formWidth, SnakeGameScene* scene)
{
    _formWidth = formWidth;
    _scene = scene;

    initListView(notes);
    initBackButton();
}

void RecordsScreen::initListView(const std::vector<Records::Note>& notes)
{
    auto recordsListView = cocos2d::ui::ListView::create();
    recordsListView->setDirection(cocos2d::ui::ScrollView::Direction::VERTICAL);
    recordsListView->setClippingEnabled(false);
    recordsListView->setPosition(cocos2d::Vec2(2, 45));

    for (const auto& note : notes)
    {
        auto& name = note.name;
        auto& score = std::to_string(note.score);

        auto button = cocos2d::ui::Button::create("", "", "");
        button->setTitleText(name + " : " + score);
        button->setTitleFontSize(2);

        recordsListView->pushBackCustomItem(button);
    }

    this->addChild(recordsListView);
}

void RecordsScreen::initBackButton()
{
    auto button = cocos2d::ui::Button::create("", "", "");
    button->setTitleText(std::move("Back"));
    button->setTitleFontSize(3);
    button->setPosition(cocos2d::Vec2(5, 47));

    button->addTouchEventListener([this](Ref* sender, cocos2d::ui::Widget::TouchEventType type)
    {
        if (type == cocos2d::ui::Widget::TouchEventType::ENDED)
        {
            auto scene = _scene;
            scene->setScreen(SnakeGameScene::Screen::Menu);
        }
    });

    this->addChild(button);
}

