//
// Created by kasperovich on 24.05.2019.
//

#include "StatusBar.h"

StatusBar* StatusBar::create(std::shared_ptr<Logger> log, std::string userName)
{
    auto* statusBar = new (std::nothrow) StatusBar();

    if (statusBar && statusBar->init())
    {
        statusBar->autorelease();

        statusBar->initStatusBar(std::move(log), std::move(userName));

        return statusBar;
    }

    delete statusBar;
    return nullptr;
}

void StatusBar::initStatusBar(std::shared_ptr<Logger> log, std::string userName)
{
    _log = std::move(log);
    createPanel();
    createScoreLabel(0);
    createUserNameLabel(std::move(userName));
}

void StatusBar::createPanel()
{
    _statusBarPanel = cocos2d::DrawNode::create();
    _statusBarPanel->drawSolidRect({ 0.0f, 50.0f }, { 50.0f, 47.0f }, cocos2d::Color4F::GRAY);
    this->addChild(_statusBarPanel, 1);

    _log->write("Create StatusBar");
}

void StatusBar::createScoreLabel(size_t countEatenApples)
{
    _scoreLabel = cocos2d::Label::createWithSystemFont("Score: " + std::to_string(countEatenApples), "Arial", 1.70f);
    _scoreLabel->setAnchorPoint(cocos2d::Vec2(0, 0));
    _scoreLabel->setPosition(1, 47.70);
    this->addChild(_scoreLabel, 2);

    _log->write("Create score Label");
}

void StatusBar::createUserNameLabel(std::string userName)
{
    _userNameLabel = cocos2d::Label::createWithSystemFont("User: " + userName, "Arial", 1.70f);
    _userNameLabel->setAnchorPoint(cocos2d::Vec2(0, 0));
    _userNameLabel->setPosition(7, 47.70);
    this->addChild(_userNameLabel,2);

    _log->write("Create userName Label");
}

void StatusBar::scoreLabelUpdate(size_t countEatenApples)
{
    _scoreLabel->setString("Score: " + std::to_string(countEatenApples * 10));
}

