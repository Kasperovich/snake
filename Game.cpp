//
// Created by kasperovich on 24.05.2019.
//

#include "Game.h"
#include <optional>
#include "SnakeGameScene.h"

static const std::string _key = "Pidor";

Game* Game::create(size_t formWidth, size_t formHeight, std::shared_ptr<Logger> log, SnakeGameScene* scene)
{
    Game* game = new (std::nothrow) Game();
    if (game && game->init())
    {
        game->autorelease();
        game->initGame(scene, std::move(log), formWidth, formHeight);
        return game;
    }

    delete game;
    return nullptr;
}

void Game::initGame(SnakeGameScene* scene, std::shared_ptr<Logger> log, size_t formWidth, size_t  formHeight)
{
    _scene      = scene;
    _log        = std::move(log);
    _formHeight = formHeight;
    _formWidth  = formWidth;

    initEventListenerKeybord();
    initField();
    initStatusBar();
    initSnake();
    initApple();
    startUpdateSnake();
}

void Game::initApple()
{
    _apple = Cell::create(cocos2d::Color4F::RED);
    _apple->setPosition(genApplePosition());
    this->addChild(_apple);

    _log->write("Apple create");
}

cocos2d::Vec2 Game::genApplePosition()
{
    _allField.clear();

    for (size_t i = 0; i < _formWidth; ++i)
    {
        for (size_t j = 0; j < _formHeight; ++j)
        {
            if (_snake->isPositionOnSnake(cocos2d::Vec2(i, j)))
            {
                _allField.emplace_back(i, j);
            }
        }
    }

    return _allField[cocos2d::random() % _allField.size()];
}

void Game::initSnake()
{
    _snake = Snake::createWithLength(5);
    this->addChild(_snake);

    _log->write("Snake create");
}

void Game::initStatusBar()
{
    auto userName = _scene->getUserName();
    _statusBar = StatusBar::create(_log, userName);
    addChild(_statusBar);

    _log->write("create statusBar");
}

void Game::initField()
{
    _direction        = cocos2d::Vec2(0, 1);
    _countEatenApples = 0;
    _speed            = 0.1;
}

void Game::initEventListenerKeybord()
{
    auto listener = cocos2d::EventListenerKeyboard::create();
    listener->onKeyPressed = CC_CALLBACK_2(Game::onKeyPressed, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

    _log->write("init event Listener Keybord");
}

void Game::startUpdateSnake()
{
    this->schedule([this](float dt) { this->updateSnake(); }, _speed, _key);

    _log->write("start snake update");
}

void Game::stopUpdateSnake()
{
    this->unschedule(_key);

    _log->write("stop snake update");
}

void Game::updateSnake()
{
    std::optional<cocos2d::Vec2> newTailPosition;

    if ((!_eatenApples.empty()) && 
        (_snake->getTailPosition() == _eatenApples.back()))
    {
        newTailPosition = _snake->getTailPosition();
    }

    _snake->moveTailToHead(_direction);

    _log->write("step Forward");

    if (_snake->isSnakeConflict())
    {
        gameOver();

        _log->write("snake Conflict");

        return;
    }

    if (newTailPosition)
    {
        addRectInTail(newTailPosition.value());

        _log->write("add Rect in Tail");
    }

    _snake->goingAbroad(_formWidth, _formHeight);

    if (_snake->getHeadPosition() == _apple->getPosition())
    {
        eatApple();
    }
}

void Game::gameOver()
{
    stopUpdateSnake();
    _isGameOver = true;

    _log->write("unshedule");

    showGameOverLabel();
}

void Game::showPauseLabel()
{
    _gameStateLabel = cocos2d::Label::createWithSystemFont("Pause", "Arial", 5);
    _gameStateLabel->setPosition(_formWidth / 2, _formHeight / 2);
    this->addChild(_gameStateLabel);
}

void Game::showGameOverLabel()
{
    _gameStateLabel = cocos2d::Label::createWithSystemFont("Game Over", "Arial", 5);
    _gameStateLabel->setPosition(_formWidth / 2, _formHeight / 2);
    this->addChild(_gameStateLabel);

    _gameStateLabel->setOpacity(0);

    auto gameOverLabelFadeTo  = cocos2d::EaseOut::create(cocos2d::FadeTo::create(1.0, 255), 1.3);
    auto gameOverLabelScaleTo = cocos2d::EaseOut::create(cocos2d::ScaleTo::create(10.0, 0.5), 1.3);
    auto spawn                = cocos2d::Spawn::createWithTwoActions(gameOverLabelFadeTo, gameOverLabelScaleTo);

    _log->write("spawn");

    cocos2d::CallFunc* menuVisible = cocos2d::CallFunc::create([=]()
    {
        auto scene = _scene;
        auto userName = scene->getUserName();

        scene->saveRecord(userName, _countEatenApples * 10);
        scene->setScreen(SnakeGameScene::Screen::Menu);
    });

    _log->write("menu visible");

    auto seq = cocos2d::Sequence::create(spawn, menuVisible, nullptr);
    _log->write("seq");
    _gameStateLabel->runAction(seq);
}

void Game::addRectInTail(cocos2d::Vec2 newTailPosition)
{
    _eatenApples.pop_back();

    auto tail = Cell::create(cocos2d::Color4F::GREEN);
    tail->setPosition(newTailPosition);
    _snake->addCellInTail(tail);
}

void Game::eatApple()
{
    _eatenApples.push_front(_apple->getPosition());

    _log->write("eat Aple");

    biteSoundPlay();

    _countEatenApples++;
    _statusBar->scoreLabelUpdate(_countEatenApples);

    _apple->setPosition(genApplePosition());
}

void Game::biteSoundPlay()
{
    auto audio = CocosDenshion::SimpleAudioEngine::getInstance();
    audio->playBackgroundMusic("apple.wav", true);
}

void Game::changeDirection(cocos2d::Vec2 newDirection)
{
    if (!_snake->isOppositeDirection(newDirection) && !_isPause)
    {
        _direction = newDirection;
    }
}

void Game::onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event)
{
    static const std::map<cocos2d::EventKeyboard::KeyCode, cocos2d::Vec2> keyCodeMap
    {
        {cocos2d::EventKeyboard::KeyCode::KEY_UP_ARROW,    { 0, 1 }},
        {cocos2d::EventKeyboard::KeyCode::KEY_DOWN_ARROW,  { 0, -1 }},
        {cocos2d::EventKeyboard::KeyCode::KEY_RIGHT_ARROW, { 1, 0 }},
        {cocos2d::EventKeyboard::KeyCode::KEY_LEFT_ARROW,  { -1, 0 }}
    };

    auto key = keyCodeMap.find(keyCode);
    if (keyCodeMap.find(keyCode) != keyCodeMap.end())
    {
        changeDirection(keyCodeMap.at(keyCode));
    }

    if (keyCode == cocos2d::EventKeyboard::KeyCode::KEY_SPACE && !_isGameOver)
    {
        pauseAndPlay();
    }
}

void Game::pauseAndPlay()
{
    if (_isPause)
    {
        this->removeChild(_gameStateLabel);
        _gameStateLabel = nullptr;
        startUpdateSnake();
    }
    else
    {
        stopUpdateSnake();
        showPauseLabel();
    }
    _isPause = !_isPause;
}


