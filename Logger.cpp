//
// Created by kasperovich on 23.05.2019.
//

#include "Logger.h"


Logger::Logger()
{
    _logfile = fopen("LOG.txt", "w+"); 
}

void Logger::write(const std::string &log)
{
    auto curTime = time(nullptr);

    fprintf(_logfile, "%s -> %s", log.c_str(), ctime(&curTime));

    std::fflush(_logfile);
}

Logger::~Logger()
{
    write("File close");
    fclose(_logfile);
}

