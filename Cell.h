//
// Created by kasperovich on 19.05.2019.
//

#ifndef MYGAME_CELL_H
#define MYGAME_CELL_H

#include <cocos/2d/CCDrawNode.h>

class Cell : public cocos2d::Node
{

public:

    static Cell* create(cocos2d::Color4F color);
};


#endif //MYGAME_CELL_H
