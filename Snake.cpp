//
// Created by kasperovich on 17.05.2019.
//

#include "Snake.h"
#include "SnakeGameScene.h"

Snake* Snake::createWithLength(size_t length)
{
    Snake* snake = new (std::nothrow) Snake();

    if (snake && snake->init())
    {
        snake->autorelease();

        for (auto y = length; y >= 1; --y)
        {
            snake->createSnakeInPosition({ 0.0f, float(y - 1) });
        }

        return snake;
    }

    CC_SAFE_DELETE(snake);
    return nullptr;
}

void Snake::createSnakeInPosition(cocos2d::Vec2 position)
{
    auto *drawNode = Cell::create(cocos2d::Color4F::GREEN);
    drawNode->setPosition(cocos2d::Director::getInstance()->getVisibleSize() * 0.5f + cocos2d::Size(position));
    this->addChild(drawNode);
    _snake.push_back(drawNode);
}

bool Snake::isSnakeConflict()
{
    return _snake.end() != std::find_if(std::next(_snake.begin()), _snake.end(), [this](const Cell* node)
    {
        return getHeadPosition() == node->getPosition();
    });
}

cocos2d::Vec2 Snake::getHeadPosition()
{
    return _snake.front()->getPosition();
}

cocos2d::Vec2 Snake::getTailPosition()
{
    return _snake.back()->getPosition();
}

void Snake::addCellInTail(Cell* tail)
{
    _snake.push_back(tail);
    this->addChild(tail);
}

void Snake::moveTailToHead(cocos2d::Vec2 vector)
{
    _snake.back()->setPosition(_snake.front()->getPosition() + vector);
    _snake.push_front(_snake.back());
    _snake.pop_back();
}

void Snake::setHeadPosition(cocos2d::Vec2 position)
{
    _snake.front()->setPosition(position);
}

void Snake::goingAbroad(const size_t formWidth, const size_t formHeight)
{
    auto snakeCutHeadPos = getHeadPosition();

    auto checkAndCut = [](float& value, float maxValue)
    {
        if (value > maxValue)
        {
            value = 0;
        }
        else if (value < 0)
        {
            value = maxValue;
        }
    };

    checkAndCut(snakeCutHeadPos.x, formWidth-1);
    checkAndCut(snakeCutHeadPos.y, formHeight);

    setHeadPosition(snakeCutHeadPos);
}

bool Snake::isPositionOnSnake(cocos2d::Vec2 position)
{
    return _snake.end() == std::find_if(_snake.begin(), _snake.end(), [&](auto && snake)
    {
        return snake->getPosition() == position;
    });
}

bool Snake::isOppositeDirection(cocos2d::Vec2 vector)
{
    auto snakeFront   = getHeadPosition() + vector;
    auto snakeSecond  = (*std::next(std::begin(_snake)))->getPosition();
    return snakeFront == snakeSecond;
}
