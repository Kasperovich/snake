#include "SnakeGameScene.h"
#include "SimpleAudioEngine.h"
#include "Snake.h"

#include <array>
#include <algorithm>
#include <iostream>
#include <ctime>


USING_NS_CC;


Scene* SnakeGameScene::createScene()
{
    return SnakeGameScene::create();
}

bool SnakeGameScene::init()
{
    _log = std::make_shared<Logger>();
    setScreen(Screen::Menu);

    _log->write("init MyMenu");

    return true;
}

void SnakeGameScene::setScreen(Screen screen)
{
    removeScreen(_currentScreen);
    initScreen(screen);
    _currentScreen = screen;
}

void SnakeGameScene::removeScreen(Screen screen)
{
    switch (screen)
    {
    case Screen::Menu:

        _menu->removeFromParent();

        break;
    case Screen::Game:

        _game->removeFromParent();

        break;
    case Screen::EnterName:

        _enterNameScreen->removeFromParent();

        break;

    case Screen::Records:

        _recordsScreen->removeFromParent();

        break;
    case Screen::None:

        break;
    default:
        break;
    }
}

void SnakeGameScene::initScreen(Screen screen)
{
    switch (screen)
    {
    case Screen::Menu:

        _menu = MyMenu::create(_formWidth, _log, this);
        this->addChild(_menu);

        break;
    case Screen::Game:

        _game = Game::create(_formWidth, _formHeight, _log, this);
        this->addChild(_game);

        break;
    case Screen::EnterName:

        _enterNameScreen = EnterNameScreen::create(_formWidth, _formHeight, _log, this);
        this->addChild(_enterNameScreen);

        break;
    case Screen::Records:

        _recordsScreen = RecordsScreen::create(_records.getNotes(), _formWidth, this);
        this->addChild(_recordsScreen);

        break;
    case Screen::None:

        break;
    default:
        break;
    }
}

void SnakeGameScene::setUserName(std::string userName)
{
    _userName = std::move(userName);
}

std::string SnakeGameScene::getUserName()
{
    return _userName ? _userName.value() : "";
}

void SnakeGameScene::saveRecord(std::string userName, std::uint32_t scope)
{
    _records.addNote(std::move(userName), scope);
}



