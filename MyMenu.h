//
// Created by kasperovich on 23.05.2019.
//
#ifndef MYGAME_MYMENU_H
#define MYGAME_MYMENU_H

#include "ui/CocosGUI.h"
#include "cocos2d.h"
#include "Logger.h"
#include "Game.h"


class SnakeGameScene;

class MyMenu : public cocos2d::Node
{
public:

    static MyMenu *create(size_t _formWidth, std::shared_ptr<Logger> log, SnakeGameScene *scene);

private:

    SnakeGameScene*      _scene;
    cocos2d::ui::Button* _enterName;
    cocos2d::ui::Button* _exit;
    cocos2d::ui::Button* _records;
    cocos2d::Label*      _menuHeader;

    std::shared_ptr<Logger> _log;


    cocos2d::ui::Button* initButton(cocos2d::Vec2 position, std::string titleText, std::function<void()> function);

    void initHeaderWithPosition(cocos2d::Vec2 position);

    void initButtons(size_t formWidth);

    void initScreen(size_t formWidth, SnakeGameScene* scene, std::shared_ptr<Logger> log);
};


#endif //MYGAME_MYMENU_H