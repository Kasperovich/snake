//
// Created by kasperovich on 17.06.2019.
//

#ifndef MYGAME_ENTERNAMESCREEN_H
#define MYGAME_ENTERNAMESCREEN_H

#include "ui/CocosGUI.h"
#include "cocos2d.h"

#include "Logger.h"

class SnakeGameScene;

class EnterNameScreen : public cocos2d::Node
{
public:

    static EnterNameScreen* create(size_t formWidth, size_t formHeight, std::shared_ptr<Logger> log, SnakeGameScene* scene);

private:

    SnakeGameScene*         _scene;
    cocos2d::ui::Button*    _startGame;
    cocos2d::ui::Button*    _backToMenu;
    cocos2d::ui::TextField* _textField;

    std::shared_ptr<Logger> _log;

    std::string userName;

    size_t _formHeight;
    size_t _formWidth;


    void initScreen(size_t formWidth, size_t formHeight, std::shared_ptr<Logger> log, SnakeGameScene* scene);

    void initTextField();

    void initButtons();

    cocos2d::ui::Button * initButton(cocos2d::Vec2 position, std::string titleText, std::function<void()> callback);
};


#endif //MYGAME_ENTERNAMESCREEN_H
