//
// Created by kasperovich on 18.06.2019.
//

#ifndef MYGAME_RECORDSSCREEN_H
#define MYGAME_RECORDSSCREEN_H

#include "cocos2d.h"
#include "ui/CocosGUI.h"

#include "Records.h"


class SnakeGameScene;

class RecordsScreen : public cocos2d::Node 
{
public:
    static RecordsScreen* create(const std::vector<Records::Note>& notes, size_t formWidth, SnakeGameScene* scene);

private:
    size_t _formWidth;

    SnakeGameScene* _scene;

    void initScreen(const std::vector<Records::Note>& notes, size_t formWidth, SnakeGameScene* scene);

    void initListView(const std::vector<Records::Note>& notes);

    void initBackButton();

};


#endif //MYGAME_RECORDSSCREEN_H
