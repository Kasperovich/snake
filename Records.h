//
// Created by kasperovich on 15.06.2019.
//

#ifndef MYGAME_RECORDS_H
#define MYGAME_RECORDS_H

#include <cocos/2d/CCNode.h>


class Records final {

public:
    struct Note
    {
        std::string     name;
        std::uint32_t   score;

        Note() = default;
        Note(std::string name, uint32_t score);
        Note(Note&&) noexcept = default;
        Note(const Note&) = default;
        Note& operator = (Note&&) = default;

        static std::pair<bool, Note> fromFile(FILE* f);
        void toFile(FILE* f) const;
    };

    Records();

    const std::vector<Note>& getNotes();
    void addNote(std::string name, std::uint32_t score);

private:
    std::vector<Note> _curNotes;
    void saveToFile();

    void sortCurNotes();

};


#endif //MYGAME_RECORDS_H
