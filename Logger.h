//
// Created by kasperovich on 23.05.2019.
//

#ifndef MYGAME_LOGGER_H
#define MYGAME_LOGGER_H

#include <cstdio>
#include "cocos2d.h"

class Logger final
{
public:

    Logger();

    ~Logger();

    void write(const std::string &log);

private:

    FILE* _logfile;
};


#endif //MYGAME_LOGGER_H
