//
// Created by kasperovich on 15.06.2019.
//

#include "Records.h"

std::pair<bool, Records::Note> Records::Note::fromFile(FILE *f)
{
    std::pair<bool, Records::Note> res;
    auto& succes = res.first;
    auto& note = res.second;
    succes = false;

    std::uint32_t nameSize;
    if (fread(&nameSize, sizeof(uint32_t), 1, f) != 1)
    {
        return res;
    }

    note.name.resize(nameSize);

    if (fread(&note.name[0], sizeof(char), nameSize, f) != nameSize)
    {
        return res;
    }

    if (fread(&note.score, sizeof(uint32_t), 1, f) != 1)
    {
        return res;
    }

    succes = true;
    return res;
}

void Records::Note::toFile(FILE *f) const
{
    std::uint32_t nameSize = name.size();
    fwrite(&nameSize, sizeof(std::uint32_t), 1, f);
    fwrite(&name[0], sizeof(char), nameSize, f);
    fwrite(&score, sizeof(std::uint32_t), 1, f);
}

Records::Note::Note(std::string name, uint32_t score)
    : name(std::move(name)), score(score)
{
}

const std::vector<Records::Note>& Records::getNotes()
{
    return _curNotes;
}

Records::Records()
{
    FILE* file = fopen("records.bin", "rb");
    if (file != nullptr)
    {
        bool isDone = false;
        while (!isDone)
        {
            auto res = Note::fromFile(file);
            auto& succes = res.first;
            auto& note = res.second;
            if (succes)
            {
                _curNotes.push_back(std::move(note));
            }
            else
            {
                isDone = true;
            }
        }
        fclose(file);
    }
}

void Records::addNote(std::string name, std::uint32_t score)
{
    _curNotes.emplace_back(name, score);
    this->saveToFile();
}

void Records::saveToFile()
{
    FILE* file = fopen("records.bin", "wb");

    sortCurNotes();

    for (const auto& note : _curNotes)
    {
        note.toFile(file);
    }
    fclose(file);
}

void Records::sortCurNotes()
{
    std::sort(_curNotes.begin(), _curNotes.end(), [](const Note& a, const Note& b)
    {
        return a.score > b.score;
    });
}

