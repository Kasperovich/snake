//
// Created by kasperovich on 24.05.2019.
//

#ifndef MYGAME_GAME_H
#define MYGAME_GAME_H


#include <cocos/2d/CCNode.h>
#include "Logger.h"
#include "Snake.h"
#include "StatusBar.h"
#include "audio\include\SimpleAudioEngine.h"

class SnakeGameScene;

class Game : public cocos2d::Node
{
public:

    static Game* create(size_t formWidth, size_t formHeight, std::shared_ptr<Logger> log, SnakeGameScene* scene);

    void startUpdateSnake();

    void stopUpdateSnake();

private:

    SnakeGameScene* _scene;
    Cell*           _apple;
    Snake*          _snake;
    StatusBar*      _statusBar;
    cocos2d::Label* _gameStateLabel;

    std::shared_ptr<Logger> _log;

    cocos2d::Vec2 _direction;

    std::deque<cocos2d::Vec2> _eatenApples;

    std::vector<cocos2d::Vec2> _allField;

    size_t _countEatenApples;

    size_t _formHeight;

    size_t _formWidth;

    bool _isPause = false;

    bool _isGameOver = false;

    float _speed;


    cocos2d::Vec2 genApplePosition();

    void onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);

    void pauseAndPlay();

    void updateSnake();

    void initEventListenerKeybord();

    void initField();

    void initStatusBar();

    void initSnake();

    void gameOver();

    void showGameOverLabel();

    void addRectInTail(const cocos2d::Vec2 newTailPosition);

    void eatApple();

    void biteSoundPlay();

    void changeDirection(cocos2d::Vec2 newDirection);

    void initGame(SnakeGameScene* scene, std::shared_ptr<Logger> log, size_t formWidth, size_t  formHeight);

    void initApple();

    void showPauseLabel();
};


#endif //MYGAME_GAME_H
