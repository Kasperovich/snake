//
// Created by kasperovich on 17.05.2019.
//

#ifndef MYGAME_SNAKE_H
#define MYGAME_SNAKE_H

#include <cocos/2d/CCDrawNode.h>

#include "cocos2d.h"
#include "Cell.h"
#include "Logger.h"

class Snake : public cocos2d::Node
{
public:

    static Snake* createWithLength(size_t length);

    cocos2d::Vec2 getHeadPosition();

    cocos2d::Vec2 getTailPosition();

    void goingAbroad(const size_t formWidth, const size_t formHeight);

    void moveTailToHead(cocos2d::Vec2 vector);

    void addCellInTail(Cell* tail);

    bool isSnakeConflict();

    bool isPositionOnSnake(cocos2d::Vec2 position);

    bool isOppositeDirection(cocos2d::Vec2 vector);

private:

    void createSnakeInPosition(cocos2d::Vec2 position);
    std::deque<Cell*> _snake;

    void setHeadPosition(cocos2d::Vec2 position);

};


#endif //MYGAME_SNAKE_H
